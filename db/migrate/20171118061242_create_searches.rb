class CreateSearches < ActiveRecord::Migration[5.1]
  def change
    create_table :searches do |t|
      t.string :keywords
      t.string :topictype
      t.string :pettype

      t.timestamps
    end
  end
end
