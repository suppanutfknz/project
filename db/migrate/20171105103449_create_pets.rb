class CreatePets < ActiveRecord::Migration[5.1]
  def change
    create_table :pets do |t|
      t.string :image_URL
      t.string :topictype
      t.string :pettype
      t.string :date
      t.text :describe
      t.text :location
      t.string :telephonenumber

      t.timestamps
    end
  end
end
