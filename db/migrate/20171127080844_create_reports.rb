class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.string :topic_url
      t.string :evidence_url

      t.timestamps
    end
  end
end
