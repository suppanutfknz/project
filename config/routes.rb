Rails.application.routes.draw do
  resources :reports
  resources :pets
  resources :searches
  get 'static_pages/about'
  root 'pets#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
