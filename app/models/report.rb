class Report < ApplicationRecord
    validates :topic_url, presence: true, url:true
    validates :evidence_url, presence: true, url:true
end
