class Pet < ApplicationRecord
    validates :image_URL, presence: true, url:true
    validates :topictype, presence: true
    validates :pettype, presence: true
    
    DATE_VALID = /\A\d+[\/]\d+[\/]\d+\z/
    validates :date, presence: true
    validates :describe, presence: true, length:{maximum:1000}
    
    validates :location, presence: true, length:{maximum:250}
    CONTACT_VALID = /\A[+-]?\d+\z/
    validates :telephonenumber, presence: true, length:{ maximum:16}, format:{ with:CONTACT_VALID,message: "Should be Number"}
    
    
    def self.search(search)
        if search
            where( ["describe LIKE ?", "%#{search}%"] ).or(where( ["Location LIKE ?", "%#{search}%"] ))
        else
            all
            
        end
    end

end
