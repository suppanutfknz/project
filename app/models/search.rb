class Search < ApplicationRecord
    
    
    TOPIC_REGEX = /Lost|Found/
    validates :topictype, presence: true, format:{with:TOPIC_REGEX}
    
    PET_REGEX = /Cat|Dog|Other/
    validates :pettype, presence: true, format:{with:PET_REGEX}
    
    def search_pets
       pets = Pet.all
       
       
       pets = pets.where( ["describe LIKE ?", "%#{keywords}%"] ).or(pets.where( ["location LIKE ?", "%#{keywords}%"] ))
       pets = pets.where( ["topictype = ?", "#{topictype}"] ) if topictype.present?
       pets = pets.where( ["pettype = ?", "#{pettype}"] ) if pettype.present?
       
       return pets
    end
    
end
