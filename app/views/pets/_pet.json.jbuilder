json.extract! pet, :id, :image_URL, :topictype, :pettype, :date, :describe, :location, :telephonenumber, :created_at, :updated_at
json.url pet_url(pet, format: :json)
