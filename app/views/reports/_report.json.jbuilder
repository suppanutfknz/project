json.extract! report, :id, :topic_url, :evidence_url, :created_at, :updated_at
json.url report_url(report, format: :json)
