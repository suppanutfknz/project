require 'test_helper'

class SearchesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @search = searches(:one)
  end
  
  test "should get new" do
    get new_search_url
    assert_response :success
  end


  test "should get show" do
    get search_url(@search)
    assert_response :success
  end
end
