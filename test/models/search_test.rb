require 'test_helper'

class SearchTest < ActiveSupport::TestCase
  def setup
    @search1 = Search.new(keywords:"siberian husky", topictype:"Found",pettype:"Dog")
  end
    
  test "should be valid" do
    assert @search1.valid?
  end
    
  test "keywords can be blank" do
    @search1.keywords = ""
    assert @search1.valid?
  end
  
  test "topictype should be present" do
    @search1.topictype = " "
    assert_not @search1.valid?
  end
  
  test "pettype should be present" do
    @search1.pettype = " "
    assert_not @search1.valid?
  end
  
  
  test "topictype should be Lost or Found" do
    @search1.topictype = "Unknown"
    assert_not @search1.valid?
    @search1.topictype = "Lost"
    assert @search1.valid?
  end
  
  
  test "pettype should be Cat or Dog or Other" do
    @search1.pettype = "Unknown"
    assert_not @search1.valid?
    @search1.pettype = "Cat"
    assert @search1.valid?
    @search1.pettype = "Other"
    assert @search1.valid?
  end
  
  
end
