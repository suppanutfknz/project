require 'test_helper'

class ReportTest < ActiveSupport::TestCase
  def setup
    @report1 = Report.new(topic_url:"https://pet.com/123",evidence_url: "https://xyz.com/myEvidence.pdf")
  end
    
  test "should be valid" do
    assert @report1.valid?
  end
  
  test "topic_url should be present" do
    @report1.topic_url = " "
    assert_not @report1.valid?
  end
  
  test "evidence_url should be present" do
    @report1.evidence_url = " "
    assert_not @report1.valid?
  end
  
  
  test "topic_url should be URL" do
    @report1.topic_url = "myEvidence.pdf"
    assert_not @report1.valid?
    
    @report1.topic_url = "https://xyz.com/myEvidence.pdf"
    assert @report1.valid?
  end
  
  
  
  test "evidence_url should be URL" do
    @report1.evidence_url = "123"
    assert_not @report1.valid?
    
    @report1.evidence_url = "https://pet.com/123"
    assert @report1.valid?
  end
  
  
end
