require 'test_helper'

class PetTest < ActiveSupport::TestCase
  def setup
    @pet1 = Pet.new(image_URL:"https://www.dog.com/dog.jpg" , topictype:"Lost",pettype:"dog",date: "5/11/2560",describe:"Cute",location:"Thailand",telephonenumber:"029998888" )
  end

  test "should be valid" do
    assert @pet1.valid?
  end
  
  
  test "image_URL should be present" do
    @pet1.image_URL = " "
    assert_not @pet1.valid?
  end
  
  test "topictype should be present" do
    @pet1.topictype = " "
    assert_not @pet1.valid?
  end
  
  test "pettype should be present" do
    @pet1.pettype = " "
    assert_not @pet1.valid?
  end
  
  test "date should be present" do
    @pet1.date = " "
    assert_not @pet1.valid?
  end
  
  test "describe should be present" do
    @pet1.describe = " "
    assert_not @pet1.valid?
  end
  
  test "location should be present" do
    @pet1.location = " "
    assert_not @pet1.valid?
  end
  
  test "PhoneNumber should be present" do
    @pet1.telephonenumber = " "
    assert_not @pet1.valid?
  end
  
  test "Image url should be URL" do
    @pet1.image_URL = "dog.jpg"
    assert_not @pet1.valid?
    
    @pet1.image_URL = "https://www.dog.com/dog.jpg"
    assert @pet1.valid?
  end
  
  test "describe should not be too long" do
    @pet1.describe = "1"*1001
    assert_not @pet1.valid?
    
    @pet1.describe = "1"*1000
    assert @pet1.valid?
  end
  
  test "location should not be too long" do
    @pet1.location = "1"*251
    assert_not @pet1.valid?
    @pet1.location = "1"*250
    assert @pet1.valid?
  end
  
  test "PhoneNumber should not be too long" do
    @pet1.telephonenumber = "1"*17
    assert_not @pet1.valid?
    
    @pet1.telephonenumber = "1"*16
    assert @pet1.valid?
  end
  
  test "PhoneNumber should be Number or Number with + - " do
    @pet1.telephonenumber = "House"
    assert_not @pet1.valid?
    
    @pet1.telephonenumber = "+669998888"
    assert @pet1.valid?
  end
end
